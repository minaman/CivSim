SOURCES=$(wildcard src/*.cpp)
OBJS=$(SOURCES:.cpp=.o)
ifeq ($(shell sh -c 'uname -s'),Linux)
  LIBFLAGS=-L. -Wl,-rpath=. -Iinclude -ltcod -ltcodxx
else
  LIBFLAGS=-Llib -ltcod-mingw-debug -static-libgcc -static-libstdc++ -mwindows
endif
CivSim : $(OBJS)
	g++ $(OBJS) -o CivSim -Wall $(LIBFLAGS) -g
src/%.o : src/%.cpp
	g++ $< -c -o $@ -Iinclude -Wall -g
