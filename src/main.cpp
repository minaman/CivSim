#include "libtcod.hpp"
#include "map.hpp"
#include "noise.hpp"
#include "aStarCosts.hpp"

#include <ctime>
#include <stdio.h>
#include <vector>
#include <cstring>
#include <math.h>
#include <list>
#include <stdlib.h>
#include <algorithm>

static const TCODColor colour_darkWall(128, 128, 128);

int i, j;

const int MAX_X = 240;
const int MAX_Y = 135;

//Making of map
Map map[MAX_X][MAX_Y];
//Making of actors
std::vector<actor> actors;

object::object(int layer_id) {
	if (layer_id == 0) { //EMPTY
		strcpy(name, "NA");
		symbol = ' ';
		passable = true;
		colour = TCODColor::white;
	}
	else if (layer_id == 1) { //TREE
		strcpy(name, "Tree");
		symbol = '^';
		passable = false;
		colour = TCODColor::darkestOrange;
	}
	else if (layer_id == 2) { //ROCK
		strcpy(name, "Rock");
		symbol = '*';
		passable = false;
		colour = TCODColor::silver;
	}
}

//Default contsuctors
object::object(){
}
tile::tile(){
}
actor::actor(){
}
Map::Map(void){
}
coordList::coordList(){
}

coordList::coordList(int x2, int y2, int parent_x2, int parent_y2){
	x = x2;
	y = y2;
	parent_x = parent_x2;
	parent_y = parent_y2;
}

tile::tile(int layer_id) {
	if (layer_id == 0) { //WATER
		strcpy(name, "Water");
		colour = TCODColor::blue;
		passable = false;
	}
	else if (layer_id == 1) { //GRASS
		strcpy(name, "Grass");
		colour = TCODColor::darkGreen;
		passable = true;
	}
	else if (layer_id == 2) { //SAND
		strcpy(name, "Sand");
		colour = TCODColor::yellow;
		passable = true;
	}
}
actor::actor(int x, int y) {
	x_loc = x;
	y_loc = y;
	coordList current;
  turns_since = 0;
  idle = true;
	strcpy(name, "TESTNAME");
}

Map::Map(int layer1_id, int layer2_id) {
	layer1 = tile(layer1_id);
	layer2 = object(layer2_id);
}

void DisplayMap(int x, int y, tile layer1, object layer2) {
	TCODConsole::root->setCharBackground(x, y, layer1.colour);
	TCODConsole::root->putChar(x, y, layer2.symbol);
	TCODConsole::root->setCharForeground(x, y, layer2.colour);
}


bool searchList(std::vector<coordList> n_list, int search_term_x, int search_term_y) {
	int size = n_list.size();
	for (i = 0; i < size; i++) {
		if ((n_list[i].x == search_term_x) && (n_list[i].y == search_term_y)){
			return true;
		}
	}
	return false;
}

bool isPassable(int x, int y) {
	if (x < 0 || y < 0 || y > MAX_Y || x > MAX_X) { //Check if values are outside of the map array
		return false;
	}
	else {
		return !(!map[x][y].layer1.passable || !map[x][y].layer2.passable);
	}
}

coordList returnParent(std::vector<coordList> n_list, int parentx, int parenty){
  int size = n_list.size() - 1;
	for (i = 0; i < size; i++) {
		if ((n_list[i].x == parentx) && (n_list[i].y == parenty)){
			return n_list[i];
		}
	}
  coordList blank = coordList(0, 0, 0, 0);
  blank.f = -1;
  return blank;
}

std::vector<coordList> pathfinding(int actor_id, int nx, int ny) {
  //Creating lists
  std::vector<coordList> openList;
  std::vector<coordList> closedList;
  std::vector<coordList> finalRoute;
  if (!isPassable(nx, ny)) {
    return finalRoute;
  }
	int x = actors[actor_id].x_loc;
	int y = actors[actor_id].y_loc;
	int dx = x;
	int dy = y;
	bool found = false;
	//Adding starting location to closed list
	coordList current = coordList(x, y, 0, 0);
	current.f = moveCostF(current.x, current.y, dx, dy, nx, ny);
	openList.push_back(current);
	closedList.push_back(current);
  finalRoute.push_back(current);
	//Adding next doors
	current = coordList(dx + 1, dy, dx, dy);
	current.f = moveCostF(current.x, current.y, dx + 1, dy, nx, ny);
	if (isPassable(current.x, current.y)){
		if ((searchList(openList, current.x, current.y) == false) && (searchList(closedList, current.x, current.y) == false)){
			openList.push_back(current);
		}
	}
	current = coordList(dx - 1, dy, dx, dy);
	current.f = moveCostF(current.x, current.y, dx - 1, dy, nx, ny);

	if (isPassable(current.x, current.y)){
		if ((searchList(openList, current.x, current.y) == false) && (searchList(closedList, current.x, current.y) == false)){
			openList.push_back(current);
		}
	}
	current = coordList(dx, dy + 1, dx, dy);
	current.f = moveCostF(current.x, current.y, dx, dy + 1, nx, ny);

	if (isPassable(current.x, current.y)){
		if ((searchList(openList, current.x, current.y) == false) && (searchList(closedList, current.x, current.y) == false)){
			openList.push_back(current);
		}
	}
	current = coordList(dx, dy - 1, dx, dy);
	current.f = moveCostF(current.x, current.y, dx, dy - 1, nx, ny);

	if (isPassable(current.x, current.y)){
		if ((searchList(openList, current.x, current.y) == false) && (searchList(closedList, current.x, current.y) == false)){
			openList.push_back(current);
		}
	}
	openList.erase(openList.begin());
	//--- NOW ALL INITIAL SQUARES HAVE BEEN CALCULATED
	bool first_time = true;
	while (found == false) {
		if (first_time == false) {
			//Add new walkable tiles
			current = coordList(dx + 1, dy, dx, dy);
			current.f = moveCostF(current.x, current.y, dx + 1, dy, nx, ny);
			if (isPassable(current.x, current.y)){
				if ((searchList(openList, current.x, current.y) == false) && (searchList(closedList, current.x, current.y) == false)){
					openList.push_back(current);
				}
			}
			current = coordList(dx - 1, dy, dx, dy);
			current.f = moveCostF(current.x, current.y, dx, dy + 1, nx, ny);
			if (isPassable(current.x, current.y)){
				if ((searchList(openList, current.x, current.y) == false) && (searchList(closedList, current.x, current.y) == false)){
					openList.push_back(current);
				}
			}
			current = coordList(dx, dy + 1, dx, dy);
			current.f = moveCostF(current.x, current.y, dx, dy + 1, nx, ny);
			if (isPassable(current.x, current.y)){
				if ((searchList(openList, current.x, current.y) == false) && (searchList(closedList, current.x, current.y) == false)){
					openList.push_back(current);
				}
			}
			current = coordList(dx, dy - 1, dx, dy);
			current.f = moveCostF(current.x, current.y, dx, dy - 1, nx, ny);
			if (isPassable(current.x, current.y)){
				if ((searchList(openList, current.x, current.y) == false) && (searchList(closedList, current.x, current.y) == false)){
					openList.push_back(current);
				}
			}
		}
		first_time = false;
		int lowest = 9999;
		int lowest_place;
		for (i = 0; i < int(openList.size()); i++) {
			if (openList[i].f < lowest){
				lowest = openList[i].f;
				lowest_place = i;
			}
		}
		closedList.push_back(openList[lowest_place]);
		dx = openList[lowest_place].x;
		dy = openList[lowest_place].y;
		if ((dx == nx) && (dy == ny)) {
			found = true;
      //Adding final destination to closed list
			current = coordList(nx, ny, dx, dy);
      current.f = 0;
      closedList.push_back(current);
      int final_x = nx;
      int final_y = ny;
      for (i = int(closedList.size()) - 1; i > 0; i--) {
        coordList to_add = returnParent(closedList, final_x, final_y);
        if (to_add.f != -1) {
          finalRoute.push_back(returnParent(closedList, final_x, final_y));
          final_x = closedList[i].parent_x;
          final_y = closedList[i].parent_y;
        }
      }
      actors[actor_id].idle = false;
      actors[actor_id].turns_since = 0;
			std::reverse(finalRoute.begin(), finalRoute.end());
      return finalRoute;
			break;
		}
		openList.erase(openList.begin() + lowest_place);
	}
	return closedList;
}

void create_river(int x, int y, int nx, int ny) {
	int dx = x;
	int dy = y;
	bool found = false;
	int lowest = 9999;
	int lowest_place = 0;
	//Creating lists
	std::vector<coordList> openList;
	std::vector<coordList> closedList;
	//Adding starting location to closed list
	coordList current = coordList(x, y, 0, 0);
	current.f = moveCostF(current.x, current.y, dx, dy, nx, ny);
	openList.push_back(current);
	closedList.push_back(current);
	//Adding next doors
	current = coordList(dx + 1, dy, dx, dy);
	current.f = moveCostF(current.x, current.y, dx + 1, dy, nx, ny);
	if (isPassable(current.x, current.y)){
		if ((searchList(openList, current.x, current.y) == false) && (searchList(closedList, current.x, current.y) == false)){
			openList.push_back(current);
			map[current.x][current.y].layer1 = tile(0);
		}
	}
	current = coordList(dx - 1, dy, dx, dy);
	current.f = moveCostF(current.x, current.y, dx - 1, dy, nx, ny);
	if (isPassable(current.x, current.y)){
		if ((searchList(openList, current.x, current.y) == false) && (searchList(closedList, current.x, current.y) == false)){
			openList.push_back(current);
			map[current.x][current.y].layer1 = tile(0);
		}
	}
	current = coordList(dx, dy + 1, dx, dy);
	current.f = moveCostF(current.x, current.y, dx, dy + 1, nx, ny);
	if (isPassable(current.x, current.y)){
		if ((searchList(openList, current.x, current.y) == false) && (searchList(closedList, current.x, current.y) == false)){
			openList.push_back(current);
			map[current.x][current.y].layer1 = tile(0);
		}
	}
	current = coordList(dx, dy - 1, dx, dy);
	current.f = moveCostF(current.x, current.y, dx, dy - 1, nx, ny);

	if (isPassable(current.x, current.y)){
		if ((searchList(openList, current.x, current.y) == false) && (searchList(closedList, current.x, current.y) == false)){
			openList.push_back(current);
			map[current.x][current.y].layer1 = tile(0);
		}
	}
	openList.erase(openList.begin());
	//--- NOW ALL INITIAL SQUARES HAVE BEEN CALCULATED
	bool first_time = true;
	while (found == false) {
		if (first_time == false) {
			//Add new walkable tiles
			current = coordList(dx + 1, dy, dx, dy);
			current.f = moveCostF(current.x, current.y, dx + 1, dy, nx, ny);
			if (isPassable(current.x, current.y)){
				if ((searchList(openList, current.x, current.y) == false) && (searchList(closedList, current.x, current.y) == false)){
					openList.push_back(current);
					map[current.x][current.y].layer1 = tile(0);
				}
			}
			current = coordList(dx - 1, dy, dx, dy);
			current.f = moveCostF(current.x, current.y, dx, dy + 1, nx, ny);
			if (isPassable(current.x, current.y)){
				if ((searchList(openList, current.x, current.y) == false) && (searchList(closedList, current.x, current.y) == false)){
					openList.push_back(current);
					map[current.x][current.y].layer1 = tile(0);
				}
			}
			current = coordList(dx, dy + 1, dx, dy);
			current.f = moveCostF(current.x, current.y, dx, dy + 1, nx, ny);
			if (isPassable(current.x, current.y)){
				if ((searchList(openList, current.x, current.y) == false) && (searchList(closedList, current.x, current.y) == false)){
					openList.push_back(current);
					map[current.x][current.y].layer1 = tile(0);
				}
			}
			current = coordList(dx, dy - 1, dx, dy);
			current.f = moveCostF(current.x, current.y, dx, dy - 1, nx, ny);
			if (isPassable(current.x, current.y)){
				if ((searchList(openList, current.x, current.y) == false) && (searchList(closedList, current.x, current.y) == false)){
					openList.push_back(current);
					map[current.x][current.y].layer1 = tile(0);
				}
			}
		}
		first_time = false;
		for (i = 0; i < int(openList.size()); i++) {
			if ((openList[i].f < lowest) && ((abs(openList[i].x - dx) == 1) || (abs(openList[i].y - dy) == 1))){
				lowest = openList[i].f;
				lowest_place = i;
			}
		}
		closedList.push_back(openList[lowest_place]);
		dx = openList[lowest_place].x;
		dy = openList[lowest_place].y;
		if ((dx == nx) && (dy == ny)) {
			break;
		}
		openList.erase(openList.begin() + lowest_place);
	}
}

std::vector<coordList> gatherResource(int actor_id, char symbol) {
	int lowest_x = 0;
	int lowest_y = 0;
	int lowest = 9999;
	for(int i=0; i<MAX_X  ; i++)
	{
		for(int j=0; j<MAX_Y ; j++)
		{
			if(map[i][j].layer2.symbol == symbol)
			{
				if (moveCostH(actors[actor_id].x_loc, actors[actor_id].y_loc, i, j) < lowest) {
					lowest_x = i;
					lowest_y = j;
					lowest = moveCostH(actors[actor_id].x_loc, actors[actor_id].y_loc, i, j);
				}
			}

		}
	}
	map[lowest_x][lowest_y].layer2.passable = true;
	map[lowest_x][lowest_y].layer1.passable = true;
	map[lowest_x][lowest_y].layer2 = object(0);
	return pathfinding(actor_id, lowest_x, lowest_y);
}

int main() {
	const int window_x = 240;
	const int window_y = 136;

	//int map_dx = ceil(MAX_X / 2);
	//int map_dy = ceil(MAX_Y / 2);
	//actor new_actor = actor(50, 50);
	//actors.push_back(new_actor);
	actor new_actor = actor(0, 2);
	actors.push_back(new_actor);
	new_actor = actor(0, 3);
	actors.push_back(new_actor);
	new_actor = actor(0, 4);
	actors.push_back(new_actor);

	//Creating tiles
	for (i = 0; i < MAX_X; i++) {
		for (j = 0; j < MAX_Y; j++) {
			if (noise(i, j) > 0 && noise(i, j) < 0.0) { //Water
				map[i][j].layer1 = tile(0);
			}
			else if (noise(i, j) <= 1) { //Grass
				map[i][j].layer1 = tile(1);
			}
			else if (noise(i, j) <= 1) { //Sand
				map[i][j].layer1 = tile(2);
			}
			map[i][j].layer2 = object(1);
		}
	}
	//Creating objects
	for (i = 0; i < MAX_X; i++) {
		for (j = 0; j < MAX_Y; j++) {
			if (noise(i, j) >= 0.3 && noise(i, j) < 0.5) { //Tree
				map[i][j].layer2 = object(1);
			}
			else if (noise(i, j) >= 0.5 && noise(i, j) < 0.6) { //Rock
				map[i][j].layer2 = object(2);
			}
			else { //Nothing
				map[i][j].layer2 = object(0);
			}
		}
	}
  create_river(20, 20, 100, 101);

  actors[1].path = pathfinding(1, 165, 93);
  actors[2].path = pathfinding(2, 124, 93);
	actors[3].path = gatherResource(3, '^');

	TCODConsole::initRoot(window_x, window_y, "CivSim v0.1", false);
	TCODConsole::setFullscreen(true);
	while (!TCODConsole::isWindowClosed()) {
		TCODConsole::root->clear();
		//Displaying tiles and objects
		for (i = 0; i < MAX_X; i++) {
			for (j = 0; j < MAX_Y; j++) {
				//DisplayMap(i, j - map_dy, map[i][j].layer1, map[i][j].layer2);
				DisplayMap(i, j, map[i][j].layer1, map[i][j].layer2);
			}
		}
		//displaying actors
		getchar();
		for (i = 0; i < int(actors.size()); i++) {
      if (actors[i].idle == false) {
        actors[i].x_loc = actors[i].path[actors[i].turns_since].x;
        actors[i].y_loc = actors[i].path[actors[i].turns_since].y;
        actors[i].turns_since++;
        if (actors[i].turns_since == int(actors[i].path.size()) - 1) {
          actors[i].idle = true;
					if (i == 3) {
						//actors[3].path = gatherResource(3, '*');
					}
        }
      }
			TCODConsole::root->putChar(actors[i].x_loc, actors[i].y_loc, 2);
			TCODConsole::root->setCharForeground(actors[i].x_loc, actors[i].y_loc, TCODColor::red);
		}


		//Screen updates only every 100 ms, used to control 'turn speed'
		//TCODSystem::sleepMilli(10);
		TCODConsole::flush();

		TCOD_key_t key = TCODConsole::checkForKeypress();
		if (key.vk == TCODK_ESCAPE) {
			return 0;
    }
		/*
		else if (TCODConsole::isKeyPressed(TCODK_UP) && map_dy > 0) {
			map_dy--;
		}
		else if (TCODConsole::isKeyPressed(TCODK_DOWN) && (map_dy < MAX_Y - 216)) {
			map_dy++;
		}
		else if (TCODConsole::isKeyPressed(TCODK_RIGHT) && (map_dx < MAX_X - 271)) {
			map_dx++;
		}
		else if (TCODConsole::isKeyPressed(TCODK_LEFT) && map_dx > 0) {
			map_dx--;
		}
    */
	}
	return 0;
}
