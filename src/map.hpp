#include <libtcod.hpp>
#include <vector>

class object {
public:
	char name[20];
	TCODColor colour;
	char symbol;
	bool passable;
	object();
	object(int layer_id);
};

class tile {
public:
	char name[20];
	TCODColor colour;
	bool passable;
	tile();
	tile(int layer_id);
};

class coordList{
public:
 	int x;
	int y;
	int parent_x;
	int parent_y;
	int f;
	coordList();
	coordList(int x2, int y2, int parent_x2, int parent_y2);
};

class actor {
public:
	bool idle;
	int turns_since;
	int x_loc;
	int y_loc;
	std::vector<coordList> path;
	char name[50];
	actor();
	actor(int x, int y);
};

class Map {
public:
	tile layer1;
	object layer2;
	Map();
	Map(int layer1_id, int layer2_id);
};

#pragma once
