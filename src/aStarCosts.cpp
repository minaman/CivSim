#include "aStarCosts.hpp"
#include <stdlib.h>

int moveCostG(int x, int y, int dx, int dy) {
	int g = (abs(dx - x) + abs(dy - y));
	return g;
}

int moveCostH(int dx, int dy, int nx, int ny){
	int h = (abs(nx - dx) + abs(dy - ny));
	return h;
}

int moveCostF(int x, int y, int dx, int dy, int nx, int ny) {
	return (moveCostG(x, y, dx, dy) + moveCostH(dx, dy, nx, ny));
}
